const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const { DateTime, Duration } = require("luxon");
const published_after = DateTime.utc()
  .minus(Duration.fromObject({ hours: 1 }))
  .toISO({ suppressMilliseconds: true });

// let query = "車禍 ";
// let api_key = "AIzaSyAJFruuvplYGxgAQfaeRngmh2TelGcFF9w";
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

app.get("/", (req, res) => {
  res.send("index2.ejs");
});

app.post("/formhandling", async (req, res) => {
  let { query, key } = req.body;
  try {
    let url = `https://www.googleapis.com/youtube/v3/search?part=snippet&q=${query}&type=video&videoDefinition=high&key=${key}`;
    // console.log(url);
    const response = await fetch(url);
    const bd = await response.json();
    res.render("index.ejs", { query, bd }); // 將 bd 傳遞給模板以供渲染
  } catch (error) {
    console.error(error);
    res.status(500).send("Internal Server Error");
  }
});

// app.get("/new", async (req, res) => {
//   try {
//     const response = await fetch(url);
//     const bd = await response.json();
//     res.render("index.ejs", { bd: bd }); // 將 bd 傳遞給模板以供渲染
//   } catch (error) {
//     console.error(error);
//     res.status(500).send("Internal Server Error");
//    }
// });

app.get("*", (req, res) => {
  res.render("error.ejs");
});

app.listen(3000, () => {
  console.log("server is running on port 3000.");
});

// const { DateTime, Duration } = require("luxon");
// const published_after = DateTime.utc()
//   .minus(Duration.fromObject({ hours: 1 }))
//   .toISO({ suppressMilliseconds: true });

// let query = "車禍";
// let api_key = "AIzaSyAJFruuvplYGxgAQfaeRngmh2TelGcFF9w";
// let url = `https://www.googleapis.com/youtube/v3/search?part=snippet&q=${query}&type=video&videoDefinition=high&key=${api_key}`;

// async function aaa() {
//   const data = await fetch(url);
//   const bd = await data.json();
//   console.log(bd);
//   bd.items.forEach((element) => {
//     console.log(
//       `標題:${element.snippet.title}，網址:https://www.youtube.com/watch?v=${element.id.videoId}，作者為${element.snippet.channelTitle}`
//     );
//   });
// }
// aaa();
